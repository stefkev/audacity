
var express = require("express");
var ffmpeg = require('fluent-ffmpeg-extended');
var async = require("async");
var multiparty = require('multiparty');
var fs = require("fs");
var archiver = require("archiver");
var jade = require("jade");
var rimraf = require("rimraf");

var app = new express;

app.set('view engine', 'jade');

app.get('/', function(req, res){
    res.render('index')
})

app.post('/upload', function(req, res){

    var form = new multiparty.Form();

    form.parse(req, function(err, fields, files) {
      // console.log(fields.originalFilename)
        // console.log(files);

        async.waterfall([
            function(cb){
                var filename = Date.now();
                fs.mkdirSync("./output/"+filename);
                fs.mkdirSync("./output/"+filename+"/project_data");
                fs.createReadStream("./example.aup").pipe(fs.createWriteStream("./output/"+filename+"/project.aup"));
                cb(null, filename)
            },
            function(filename, callback){
                var cnt = 0;
                // console.log(fields)
                // console.log(files)
                async.eachSeries(files.data, function(file, cb){
                    async.series([
                        function(cb){
                            var original = file.originalFilename;
                            var destilated = original.substr(0, original.lastIndexOf('.')) || original;
                            // console.log('output file'+"./output/"+filename+"/project_data/"+destilated+".ogg")
                            var proc = new ffmpeg({source:file.path, priority:10})
                                .withAudioCodec('libvorbis')
                                .toFormat('ogg')
                                .saveToFile("./output/"+filename+"/project_data/"+destilated+".ogg", function(stdout, stderr){
                                    // console.log("stdout \n"+stdout);
                                    // console.log("stderr\n"+stderr);
                                    //if(err){
                                    //    console.log("err\n"+err)
                                    //    throw err;
                                    //}
                                    fs.appendFileSync('./output/'+filename+'/project.aup', '\n\t<import filename="'+destilated+'.ogg"' +
                                        ' offset="0.00000000" mute="0" solo="0" height="150" minimized="0" gain="1.0" pan="0.0"/>')
                                    cb(destilated)
                                })

                        },
                        function(destilated, cb){
                            console.log('outputting to file')
                            console.log(destilated)

                            console.log("appended")
                            cb();
                        }
                    ], function(){
                        cb()
                    })
                }, function(){
                    fs.appendFileSync('./output/'+filename+'/project.aup', '\n</project>')
                    callback(null, filename);

                })
            }, function(filename, callback){
                console.log("deflating!")
                var archive = archiver('zip');
                archive.on('error', function(err) {
                    res.status(500).send({error: err.message});
                });

                archive.on('end', function() {
                    console.log('Archive wrote %d bytes', archive.pointer());
                    res.end();
                    callback()
                });

                res.on("finish", function(){
                    console.log("Finished response")
                    rimraf.sync('./output/'+filename)

                })
                res.attachment('project.zip');
                archive.pipe(res);
                archive.directory('./output/'+filename);
                archive.finalize();




            }
        ])
    });

    form.on('close', function(){
        console.log("uploaded")
    })
})


app.listen(3000, function(){
    console.log("Audacity exporter started on port 3000");
})
